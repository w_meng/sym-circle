## A creative way to render a circle
To set it up, clone the repo to your local machine
Download the Gastby CLI `npm install -g gatsby-cli`
Navigate to the `\circle` directory
Run `gatsby develop` and the website will be hosted on `localhost:8000`

Or, you can see it live at `https://symcirclemaster.gtsb.io`
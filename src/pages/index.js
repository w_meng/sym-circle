import React from "react"
import {useState} from "react"

function distance(x1, y1, x2, y2) {
  return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
}

function Circle({radius}) {
  radius = Number(radius);
  const width = radius * 2 + 1;
  const height = radius * 2 + 1;
  const centerX = radius;
  const centerY = radius;
  let grid = [];
  
  for (let y = 0; y < height; y++) {
    let row = [];
    for (let x = 0; x < width; x++) {
      const dist = distance(x, y, centerX, centerY);
      if (Math.round(dist) == radius) {
        row.push('00');
      }
      else {
        row.push('__');
      }
    }
    row.push("\n");
    grid.push(row.join(''));
  }
  return (
    <div>
      {grid.map((s, idx) => <div key={idx}>{s}</div>)}
    </div>
  );
}


export default function Home() {
  const [inputRadius, setInputRadius] = useState(0);
  const [actualRadius, setActualRadius] = useState(0);

  return (
    <div>
      <form onSubmit={(event) => {
        setActualRadius(inputRadius);
        event.preventDefault();
      }}>
        <label>
          Set radius:
          <input type='number' value={inputRadius} onChange={(event)=>setInputRadius(event.target.value)} />
        </label>
        <input type='submit' value='Submit'/>
      </form>
      Current radius: {actualRadius}
      <Circle radius={actualRadius} />
    </div>
  );
}
